/***********************************************************************
 * LapaVerde
 *
 * Sistema de control para un vehiculo seguidor de lineas
 **********************************************************************/

/* *********************************************************************
 * Librerias para modularizar los componentes:
 *  ControlMotores facilita controlar los motores en base a una
 * direccion.
 *  SensorLinea facilita medir los sensores de color y determinar la
 * posicion del carro con respecto a la linea.
 *  PID y PID_Autotune es el control central del sistema, determina en
 * que direccion mover los motores segun la posicion medida por el
 * sensor.
 * */
#include <ControlMotores.h>
#include <SensorLinea.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>


/* *********************************************************************
 * Variables globales
 * */
bool modo;
unsigned long t;
unsigned long t0;
int dt = 2000;
int led = LED_BUILTIN;
//Pin para el swicth de calibracion
int pin_cal = 52;
//Variables de control
double Objetivo, Posicion, Direccion, Poder;
bool Sentido;
//Crea componentes del sistema
SensorLinea sensor(&Posicion, A0, A1, A2, A3, A4, A5, A6);
ControlMotores motores(&Direccion, &Poder, &Sentido, 5, 4, 3, 2);
PID control(&Posicion, &Direccion, &Objetivo, 0.9*85, 0.1*85, 1.5*85, DIRECT);


/* *********************************************************************
 * Configuración inicial
 * Establece los valores de las variables de control constantes y coloca
 * los pines en el estado correcto
 * */
void setup() {
    Serial.begin(9600);
    pinMode(led, OUTPUT);
    pinMode(pin_cal, INPUT);
    Poder = 125;
    Objetivo = 0;
    Sentido = true;
    control.SetOutputLimits(-255, 255);
    control.SetSampleTime(1);
    control.SetMode(AUTOMATIC);
}


/* =====================================================================
 * Funcion de debugeo
 * */
void debug() {
    Serial.println("Iniciando debug.");
    Serial.println("------------------Principal------------- ---------");
    Serial.print("Modo ");
    if (modo)
    {
        Serial.println("calibrando");
    }
    else
    {
        Serial.println("corriendo");
    }
    //~ Serial.println("------------------Sensores-----------------------");
    //~ sensor.debug();
    Serial.println("------------------Motores------------------------");
    motores.debug();
    Serial.println("------------------Control------------------------");
    Serial.print("Constantes de Proporcionalidad : ");
    Serial.print(control.GetKp());
    Serial.print(" - ");
    Serial.print(control.GetKi());
    Serial.print(" - ");
    Serial.println(control.GetKd());
    Serial.print("Posicion: ");
    Serial.println(Posicion);
    Serial.print("Direccion: ");
    Serial.println(Direccion);
    Serial.println("==================================================");
}


/* *********************************************************************
 * Ciclo principal
 * Determina el estado del switch para colocar al vehiculo en modo de
 * calibracion o conduccion.
 * En modo de conducción usa el sensor de linea para medir la Posicion,
 * lo envia al control PID para determinar la Direccion a la que deben
 * moverse los motores
 * */
void loop() {
    modo = digitalRead(pin_cal) == HIGH;
    if (modo)
    {
        // Indica que el vahiculo esta calibrando
        digitalWrite(led, HIGH);
        // Parar Motores
        motores.Parar();
        // Modo de calibracion
        sensor.Calibrar();
    }
    else
    {
        // Indica que el vahiculo esta corriendo
        digitalWrite(led, LOW);
        // Activar Motores
        motores.Correr();
        // Modo de conducción
        sensor.Medir();
        control.Compute();
        motores.Ajustar();
    }
    // Debugea cada tanto tiempo
    t = millis();
    if (t - t0 >= dt)
    {
        t0 = t;
        debug();
    }
}
