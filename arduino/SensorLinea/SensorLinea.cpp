/***********************************************************************
 * SensorLinea.cpp - Libreria para controlar una tarjeta sensor de
 * una linea.
 * Creado por Wolfang Torres, Noviembre 2016.
 * Liberado al dominio publico.
 **********************************************************************/

#include "Arduino.h"
#include "SensorLinea.h"


/* *********************************************************************
 * Variables globales
 * */
int _valores_calibracion[7][2] = {{0, 1023}, {0, 1023}, {0, 1023},
    {0, 1023}, {0, 1023}, {0, 1023}, {0, 1023}};
int _referencia[7] = {511, 511, 511, 511, 511, 511, 511};

/* *********************************************************************
 * Posicion es el pointer a la variable de control,
 *
 * pinA al pinG son los puertos donde se conectan los sensores que
 * detectan la linea, van desde el mas izquiedo al mas derecho
 * */
SensorLinea::SensorLinea(double* Posicion, int pinA, int pinB, int pinC,
    int pinD, int pinE, int pinF, int pinG)
{
    posicion = Posicion;
    int _puertos[7] = {pinA, pinB, pinC, pinD, pinE, pinF, pinG};
    for (int i = 0; i <= 6; i++)
    {
        pinMode(_puertos[i], INPUT);
    }
}


/* *********************************************************************
 * Mide los sensores y guarda los valores mas altos y bajos que detectan
 * cada uno, con el objetivo de determinar un valor de referencia.
 *
 * Si se estaba midiendo antes regresa los valores al defecto para poder
 * calibrar desde cero.
 *
 * Siempre se debe calibrar antes de medir para poder adjustarse a las
 * direfentes condiciones de luz y colores de linea
 * */
void SensorLinea::Calibrar()
{
    // empesar a calibrar por primera ves o despues de haber medido
    if (not _calibrando)
    {
        for (int i = 0; i <= 6; i++)
        {
            _valores_calibracion[i][0] = 0;
            _valores_calibracion[i][1] = 1023;
            _referencia[i] = 511;
        }
        _calibrando = true;
    }
    // buscar valores maximos y minimos
    int val = 0;
    for (int i = 0; i < 7; i++)
    {
        val = analogRead(_puertos[i]);
        if (val > _valores_calibracion[i][0])
            _valores_calibracion[i][0] = val;
        if (val < _valores_calibracion[i][1])
            _valores_calibracion[i][1] = val;
    }
}


/* *********************************************************************
 * Mide los valores de los sensores para detectar la posicion de la
 * linea
 *
 * Si estaba antes calibrando detiene la calibración y crea os valores
 * de referencia.
 *
 * Determina si un sensor esta activo basado si su lectura con respecto
 * al valor de referencia.
 *
 * Determina la ubicacion de la linea al terminar cual es el sensor
 * activo mas cercano a la ubicacion anterior, de esa forma pude ignorar
 * lineas extras y comportarse mejor frente a interrupciones de la linea
 * */
void SensorLinea::Medir()
{
    // medir por primera ves depues de calibrar
    if (_calibrando)
    {
        for (int i = 0; i < 7; i++)
        {
            _referencia[i] = ((_valores_calibracion[i][0] + _valores_calibracion[i][1])/2) + 30;
        }
        _calibrando = false;
    }
    // leer los sensores
    for (int i = 0; i < 7; i++)
    {
        _valores[i] = (analogRead(_puertos[i]) > _referencia[i]);
        Serial.print("Sensor ");
        Serial.print(i-3);
        Serial.print(" ");
        Serial.println(_valores[i]);
    }
    // encuentra la continuacion de la linea
    _encontrado = false;
    int val = 0;
    for (int d = 0; d < 7; d++)
    {
        for (int i = 6; i >= 0; i--)
        {
            // determina si lee de izq. a der. o al reves
            if (_camino_izquierda)
            {
                i = 6 - i
            }
            // escoje el sensor activo mas cercano a la ultima pos de la
            // linea
            if (_valores[i] == true)
            {
                val = *posicion - (i-3);
                if (abs(val) <= d)
                {
                    *posicion = i-3;
                    _encontrado = true;
                    break;
                }
            }
        }
        if (_encontrado)
            break;
    }
    // encontrar marcas en el camino
    int vi = *posicion - 2;
    int vd = *posicion + 2;
    if (!_bifurcacion && (_valores[vi] && _valores[vd]))
    {
        _bifurcacion = true;
        _marca = true;
        _ti = millis();
        // por defecto ir a la derecha
        _camino_izquierda = false;
    }
    else if (_bifurcacion && _marca && !(_valores[vi] || _valores[vd]))
    {
        _marca = false;
        _tf = millis();
        _largo_marca = _tf - _ti;
    }
    else if (_bifurcacion && !_marca)
    {
        // asegurarse de que sea marcas de entrada a una bifurcacion y
        // no en una salida
        long t = millis() - _tf;
        if (t <= _largo_marca)
        {
            if (_valores[vi] || _valores[vd])
            {
                if (_valores[vi])
                {
                    _camino_izquierda = true;
                }
                else if (_valores[vd])
                {
                    _camino_izquierda = false;
                }
                _bifurcacion = false;
            }
        }
        else
        {
            _bifurcacion = false;
        }
    }
}
