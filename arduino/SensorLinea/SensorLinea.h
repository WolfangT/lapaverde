/***********************************************************************
 * SensorLinea.h - Libreria para controlar una tarjeta sensor de linea.
 * Creado por Wolfang Torres, Noviembre 2016.
 * Liberado al dominio publico.
 **********************************************************************/

#ifndef SensorLinea_h
#define SensorLinea_h

#include "Arduino.h"

class SensorLinea
{
    public:
        SensorLinea::SensorLinea(double* Posicion, int pinA, int pinB, int pinC,
            int pinD, int pinE, int pinF, int pinG);
        void Medir();
        void Calibrar();
    private:
        // variable de control
        double *posicion;
        // medicion
        bool _camino_derecha;
        int _puertos[7];
        bool _encontrado;
        bool _valores[7];
        // calibracion
        int _valores_calibracion[7][2];
        int _referencia[7];
        bool _calibrando;
        // bifurcaciones
        bool _bifurcacion;
        bool _marca;
        long _ti;
        long _tf;
        long _largo_marca;
};

#endif
