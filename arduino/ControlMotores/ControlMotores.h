/***********************************************************************
 * ControlMotores.cpp - Libreria simple para controlar un par de motores
 * dc, uno al lado del otro, que mueven un vehiculos.
 * Creado por Wolfang Torres, Noviembre 2016.
 * Liberado al dominio publico.
 **********************************************************************/

#ifndef ControlMotores_h
#define ControlMotores_h

#include "Arduino.h"

class ControlMotores
{
    public:
        ControlMotores::ControlMotores(double* Direccion, double* Poder, bool* Sentido,
            int pinA, int pinB, int pinC, int pinD);
        void Ajustar();
        void Correr();
        void Parar();
    private:
        double *direccion;
        double *poder;
        bool *sentido;
        bool _correr;
        int _motor_izquierda_p;
        int _motor_izquierda_n;
        int _motor_derecha_p;
        int _motor_derecha_n;
};

#endif
