/***********************************************************************
 * ControlMotores.cpp - Libreria simple para controlar un par de motores
 * dc, uno al lado del otro, que mueven un vehiculos.
 * Creado por Wolfang Torres, Noviembre 2016.
 * Liberado al dominio publico.
 **********************************************************************/

#include "Arduino.h"
#include "ControlMotores.h"


/* *********************************************************************
 * Direccion, Poder y Sentido son pointers a las variables de control
 *
 * pinA es el control para el giro positivo del motor izquierdo
 * pinB es el control para el giro negativo del motor izquierdo
 * pinC es el control para el giro positivo del motor derecho
 * pinC es el control para el giro negativo del motor derecho
 * */
ControlMotores::ControlMotores(double* Direccion, double* Poder, bool* Sentido,
    int pinA, int pinB, int pinC, int pinD)
{
    direccion = Direccion;
    poder = Poder;
    sentido = Sentido;
    pinMode(pinA, OUTPUT);
    pinMode(pinB, OUTPUT);
    pinMode(pinC, OUTPUT);
    pinMode(pinD, OUTPUT);
    _motor_izquierda_p = pinA;
    _motor_izquierda_n = pinB;
    _motor_derecha_p = pinC;
    _motor_derecha_n = pinD;
}

/* *********************************************************************
 * Ajusta el poder a los motores segun las variabels de control:
 *
 * Direccion es un numero entre -255 y 255 que determina la proporcion
 * de poder entre los motores, Numeros negativos causan que el motor
 * izquierdo sea mas rapido que el derecho, numeros positivos los contrario
 *
 * Poder en un numero entre 0 y 255 indicando cual es el poder que se
 * suministrara al motor mas rapido
 *
 * Sentido es un bool el cual si es positivo indica que el vehiculo va
 * hacia adelante y si es nagativo que va hacia atras, invirtiendo los
 * motores
 * */
void ControlMotores::Ajustar()
{
    double _direccion = *direccion;
    double _poder = *poder;
    bool _sentido = *sentido;
    double _izquierda = 255 - _direccion;
    double _derecha = 255 + _direccion;
    if (_izquierda > 255)
        _izquierda = 255;
    if (_derecha > 255)
        _derecha = 255;
    _izquierda = map(_izquierda, 0, 255, 0, _poder);
    _derecha = map(_derecha, 0, 255, 0, _poder);
    if (_correr)
    {
        if (_sentido)
        {
            analogWrite(_motor_izquierda_p, _izquierda);
            analogWrite(_motor_derecha_p, _derecha);
        }
        else
        {
            analogWrite(_motor_derecha_n, _izquierda);
            analogWrite(_motor_izquierda_n, _derecha);
        }
    }
}

/* *********************************************************************
 * Idica a los motorres que pueden moverse
 * */
void ControlMotores::Correr()
{
    _correr = true;
}

/* *********************************************************************
 * Idica a los motorres que deben pararse
 * */
void ControlMotores::Parar()
{
    _correr = false;
}
